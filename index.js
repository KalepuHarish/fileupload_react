const express = require('express')
const path = require('path')
const app = express()
const port = 3000

console.log(path.join(__dirname));
app.get(express.static(path.join(__dirname, 'public')))
app.use('/', express.static('./dist', {
    index: "index.html"
  }))
//app.use('/static', express.static(path.join(__dirname, 'public')))

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
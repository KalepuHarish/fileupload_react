const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const webpack = require('webpack')

module.exports = {
  optimization: {
    minimizer: [
      new TerserPlugin(),
      new webpack.LoaderOptionsPlugin({
        minimize: true
      })
    ],
  },
  plugins: [
    new OptimizeCssAssetsPlugin(),
    new TerserPlugin()
  ],
};
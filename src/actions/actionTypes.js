export const AJAX_BEGIN = 'AJAX_BEGIN'
export const AJAX_END = 'AJAX_END'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGIN_FAILURE = 'LOGIN_FAILURE'
export const USER_OBJECT= 'USER_OBJECT'
export const IS_FETCHING = 'IS_FETCHING'

export const UPLOAD_SUCCESS = 'UPLOAD_SUCCESS'
export const UPLOAD_FAIL = 'UPLOAD_FAIL'
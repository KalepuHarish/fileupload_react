import axios from 'axios';
import history from "../helpers/history";
import { PURGE } from 'redux-persist';
const baseUrl = "http://localhost:8080"
const request = axios.create({
    //baseURL: "http://52.203.248.116:8080/aepliot",
    //mode: 'no-cors',
    // headers: {
    //     'Access-Control-Allow-Origin': '*',
    //     'Content-Type': 'application/json',
    // }
});
import {
    LOGIN_SUCCESS, LOGIN_FAILURE, IS_FETCHING,
    UPLOAD_FAIL, UPLOAD_SUCCESS, AJAX_BEGIN, AJAX_END,
} from './actionTypes'

export function logout() {

    return function (dispatch) {
        dispatch({
            type: PURGE,
            key: "root",
            result: () => null
        });
        history.push('/')
    }
}

export function login(object) {

    return function (dispatch) {
        dispatch({ type: AJAX_BEGIN })
        dispatch({ type: IS_FETCHING })
        return request.post(baseUrl + '/api/user/auth', object)
            .then(function (response) {
                if (response.data.status === 200) {
                    dispatch({ type: LOGIN_SUCCESS, user: response.data })
                    history.push('/dashboard');
                } else {
                    dispatch({ type: LOGIN_FAILURE, message: response.data })
                }
                dispatch({ type: AJAX_END })
                return response.data;
            })
            .catch(function () {
                dispatch({ type: AJAX_END })
            })
    }
}



export function upload(file) {
    return function (dispatch) {
        dispatch({ type: AJAX_BEGIN })
        return request.post(baseUrl + '/api/upload', file)
            .then(function (response) {
                if (response.data.status === 200) {
                    dispatch({ type: UPLOAD_SUCCESS, list: response.data })
                } else {
                    dispatch({ type: UPLOAD_FAIL, message: response.data })
                }
                dispatch({ type: AJAX_END })
                return response.data;
            })
            .catch(function () {
                dispatch({ type: AJAX_END })
            })
    }
}

export function getData(value) {
    return function (dispatch) {
        dispatch({ type: AJAX_BEGIN })
        return request.get(baseUrl + '/api/data?value='+value)
            .then(function (response) {
                if (response.data.status === 200) {
                    dispatch({ type: UPLOAD_SUCCESS, list: response.data })
                } else {
                    dispatch({ type: UPLOAD_FAIL, message: response.data })
                }
                dispatch({ type: AJAX_END })
                return response.data;
            })
            .catch(function () {
                dispatch({ type: AJAX_END })
            })
    }
}


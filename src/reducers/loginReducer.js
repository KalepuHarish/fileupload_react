import { LOGIN_SUCCESS, LOGIN_FAILURE, USER_OBJECT, IS_FETCHING } from '../actions/actionTypes'
import { PURGE } from 'redux-persist';
const initialState = {
    loggedIn: true,
    isFetching: false,
    user: {},
    message: '',
    list: []
}
export default function ntrsReducer(state = initialState, action) {
    switch (action.type) {
        case IS_FETCHING:
            let _state = Object.assign({}, state, {
                isFetching: true,
            });
            return _state;
        case LOGIN_SUCCESS:
            _state = Object.assign({}, state, {
                loggedIn: true,
                user: action.user,
                message: action.message,
                isFetching: false
            });
            return _state;
        case LOGIN_FAILURE:
            return {
                loggedIn: false,
                message: action.message,
                isFetching: false
            };
        case USER_OBJECT:
            let userObject = Object.assign({}, state, {
                userObject: action.userDetails
            })
            return userObject;
        case PURGE:
            return initialState;
        default:
            return state
    }
}
import { UPLOAD_FAIL, UPLOAD_SUCCESS } from '../actions/actionTypes'
import { PURGE } from 'redux-persist';
const initialState = {
    message: '',
    list: []
}
export default function ntrsReducer(state, action) {
    if (state == undefined) {
        state = initialState
    }
    switch (action.type) {
        case UPLOAD_SUCCESS:
            let _state = Object.assign({}, state);
            _state['list'] = action.list.result
            return _state;
        case UPLOAD_FAIL:
            return {
                message: action.message
            };
        case PURGE:
            return initialState;
        default:
            return state
    }
}
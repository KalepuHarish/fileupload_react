import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import fileupload from './FileUpload'
import auth from './loginReducer'

const  rootReducer = combineReducers({
  fileupload: fileupload,
  auth: auth,
  router: routerReducer
});

export default rootReducer;
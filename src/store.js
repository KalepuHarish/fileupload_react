import { createStore, applyMiddleware, combineReducers } from 'redux';
import { createLogger } from 'redux-logger';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import { routerMiddleware, routerReducer } from "react-router-redux";
import thunk from 'redux-thunk';
import { createBrowserHistory } from 'history'
import ntrs from './reducers';
import { persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

const persistConfig = {
  key: 'root',
  storage,
}

const logger = createLogger({
  collapsed: true,
  diff: true
});

const reducerFinal = combineReducers({
  ntrs,
  routing: routerReducer
});

export const history = createBrowserHistory();
const middleware = routerMiddleware(history);

const getMiddleware = () => {
  if (process.env.NODE_ENV === 'production') {
    return applyMiddleware(middleware, thunk);
  }
  return applyMiddleware(middleware, logger, thunk);
};

const persistedReducer = persistReducer(persistConfig, reducerFinal)

export default createStore(persistedReducer, composeWithDevTools(getMiddleware()));
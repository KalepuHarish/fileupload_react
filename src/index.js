import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import { ConnectedRouter } from "react-router-redux";
import history from "./helpers/history";
import store from './store';
import { persistStore } from 'redux-persist'
import { PersistGate } from 'redux-persist/integration/react';
import Layout from './components/layout/layout';
import App from './components/app';

if (module.hot) {
  module.hot.accept('./reducers', () =>
    store.replaceReducer(require('./reducers')/*.default if you use Babel 6+ */)
  );
}

let persistor = persistStore(store)

ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <ConnectedRouter history={history}>
        <Switch>
          <Route exact path="/" component={App} />
          <Route path="/home" component={Layout} />
          <Route path="*" component={Layout} />
        </Switch>
      </ConnectedRouter>
    </PersistGate>
  </Provider>, document.getElementById('app')
);
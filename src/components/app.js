import React, { Component } from 'react'
import { connect } from 'react-redux';
import * as actionCreators from '../actions/ntrs';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import $ from 'jquery';
import './app.css';
class App extends Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)

    this.state = {
      id: '',
      password: '',
      submitted: false,
      type: 'password',
    };
    this.showHide = this.showHide.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(e) {
    e.preventDefault();
    this.setState({ submitted: true });
    $('#cover-spin').show(0)
    const object = {
      userId: this.refs.id.value,
      password: this.refs.password.value
    }
    if (object.userId && object.password) {
      $('#cover-spin').show(0)
      setTimeout(() => {
        this.props.login(object);
        $('#cover-spin').hide(0)
      }, 1000)
    } else {
      //empty
    }
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }
  
  showHide(e){
    //e.preventDefault();
    //e.stopPropagation();
    this.setState({
      type: this.state.type === 'input' ? 'password' : 'input'
    })  
  }
  render() {
    const { loggedIn, message } = this.props;
    const { id, password, submitted } = this.state;
    return (
      <div className="login-container">
        <div className="container-fluid bg-login-image">
          <div className="row">
            {/* <img src="../public/img/left_bg.svg" alt="contest-cover" /> */}
            <div className="col-lg-7 login-left-child">
              {/* <img src="../public/img/water_icon_left.svg" alt="Contest-cover" className="login-left-logo" /> */}
              {/* <h3 className="left-hand-child-text1">  Get more things done with AEPL</h3> */}
              {/* <small className="left-hand-child-text2">A Clean Drinking Water ATM solution. With uses the Internet of Things</small> */}
            </div>
            <div className="col-lg-5 app-login-side">
              <div className="p-5">
                <div>
                  <img src="" alt="" />
                  <h4 className="login-page-h4 h4 text-gray-900 mb-4">Welcome back! Please login to your account.</h4>
                </div>
                <form className="user" onSubmit={this.onSubmit.bind(this)}>
                  <div className="form-group">
                    <input type="text" className="form-control form-control-user" name='id' id="exampleInputEmail" ref="id" onChange={this.handleChange} aria-describedby="emailHelp" placeholder="Enter Employee-ID" />
                    {submitted && !id &&
                      <div className="form-group has-error">
                        <label className="control-label" htmlFor="inputError"><i className="fa fa-times-circle-o"></i>Employee-ID is required</label>
                      </div>
                    }
                  </div>
                  <div className="form-group pswd_form_input">
                    <input type={this.state.type} className="form-control form-control-user" name="password" id="exampleInputPassword" placeholder="Password" ref="password" onChange={this.handleChange} />
                    <span onClick={this.showHide} className="pswdhide_icon">{this.state.type === 'input' ? <i class="far fa-eye-slash"></i> : <i class="far fa-eye"></i> }</span>
                    
                    {submitted && !password &&
                      <div className="form-group has-error">
                        <label className="control-label" htmlFor="inputError"><i className="fa fa-times-circle-o"></i>Password is required</label>
                      </div>
                    }
                  </div>
                  <ul className="login-remeber">
                    <li>
                      <div className="form-group">
                        <div className="custom-control custom-checkbox small">
                          <input type="checkbox" className="custom-control-input" id="customCheck" />
                          <label className="custom-control-label" htmlFor="customCheck">Remember Me</label>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div className="text-rght">
                        <a className="small" href="forgot-password.html">Forgot Password?</a>
                      </div>
                    </li>
                  </ul>

                  <button type="submit" className="btn btn-primary btn-user btn-block"><b>Login</b></button>
                  <p className="login-btn-btm-text">Not having credentials. Request for access.</p>
                  {submitted && !loggedIn &&
                    <div className="form-group has-error">
                      <label className="control-label" htmlFor="inputError"><i className="fa fa-times-circle-o"></i>{message.message}</label>
                    </div>
                  }
                  {/* <hr>
                      <a href="index.html" className="btn btn-google btn-user btn-block">
                        <i className="fab fa-google fa-fw"></i> Login with Google
                      </a>
                      <a href="index.html" className="btn btn-facebook btn-user btn-block">
                        <i className="fab fa-facebook-f fa-fw"></i> Login with Facebook
                      </a> */}
                </form>
                {/* <div className="text-center">
                        <a className="small" href="register.html">Create an Account!</a>
                      </div> */}
                <div className="login-footer">
                  <p>By clicking this button, you agree to FileUpload <a href="#">Terms of Service</a>  and <a href="#">Privacy Policy</a>.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}


const mapStateToProp = (state) => {
  return {
    user: state.ntrs.auth.user,
    loggedIn: state.ntrs.auth.loggedIn,
    message: state.ntrs.auth.message,
    isFetching: state.ntrs.auth.isFetching
  }
};

export default connect(mapStateToProp, actionCreators)(App);
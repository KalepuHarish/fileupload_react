import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Link } from 'react-router-dom'
import $ from 'jquery'

class LeftMenu extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        $("#sidebarToggle, #sidebarToggleTop").on("click", function () {
            $("body").toggleClass("sidebar-toggled"), $(".sidebar").toggleClass("toggled"), $(".sidebar").hasClass("toggled") && $(".sidebar .collapse").collapse("hide")
        }), $(window).resize(function () {
            $(window).width() < 768 && $(".sidebar .collapse").collapse("hide")
        }), $(document).on("scroll", function () {
            100 < $(this).scrollTop() ? $(".scroll-to-top").fadeIn() : $(".scroll-to-top").fadeOu$()
        }), $(document).on("click", "a.scroll-to-top", function () {
            var e = $(this);
            $("html, body").stop().animate({
                scrollTop: $(e.attr("href")).offse$().top
            }, 1e3, "easeInOutExpo")
        }), $(window).resize(function () {
            if ($(window).width() < 768) {
                $('.sidebar .collapse').collapse('hide');
            }
        })
        this.linkClicked()
    }
    linkClicked() {
        $(function () {
            //alert(location)
            if (/organization/.test(location)) {
                $(".collapse-item").removeClass("reportsActive")
                $('#organization').addClass('reportsActive');
            } else if (/department/.test(location)) {
                $(".collapse-item").removeClass("reportsActive")
                $('#department').addClass('reportsActive');
            } else if (/designation/.test(location)) {
                $(".collapse-item").removeClass("reportsActive")
                $('#designation').addClass('reportsActive');
            } else if (/employee/.test(location)) {
                $(".collapse-item").removeClass("reportsActive")
                $('#employee').addClass('reportsActive');
            } else if (/cluster/.test(location)) {
                $(".collapse-item").removeClass("reportsActive")
                $('#cluster').addClass('reportsActive');
            } else if (/motherplant/.test(location)) {
                $(".collapse-item").removeClass("reportsActive")
                $('#motherplant').addClass('reportsActive');
            }
            else if (/rdu/.test(location)) {
                $(".collapse-item").removeClass("reportsActive")
                $('#rdu').addClass('reportsActive');
            }
            else if (/smartcard/.test(location)) {
                $(".collapse-item").removeClass("reportsActive")
                $('#smartcard').addClass('reportsActive');
            }
            else if (/wallet/.test(location)) {
                $(".collapse-item").removeClass("reportsActive")
                $('#wallet').addClass('reportsActive');
            }
            else if (/recharge/.test(location)) {
                $(".collapse-item").removeClass("reportsActive")
                $('#recharge').addClass('reportsActive');
            }
            else if (/block/.test(location)) {
                $(".collapse-item").removeClass("reportsActive")
                $('#block').addClass('reportsActive');
            }
            else if (/card/.test(location)) {
                $(".collapse-item").removeClass("reportsActive")
                $('#cblock').addClass('reportsActive');
            }
            else if (/sales/.test(location)) {
                $(".collapse-item").removeClass("reportsActive")
                $('#sales').addClass('reportsActive');
            }
            else if (/customer/.test(location)) {
                $(".collapse-item").removeClass("reportsActive")
                $('#customer').addClass('reportsActive');
            }
            else if (/cip/.test(location)) {
                $(".collapse-item").removeClass("reportsActive")
                $('#cip').addClass('reportsActive');
            }
            else if (/waterFill/.test(location)) {
                $(".collapse-item").removeClass("reportsActive")
                $('#waterFill').addClass('reportsActive');
            }
            else if (/rReport/.test(location)) {
                $(".collapse-item").removeClass("reportsActive")
                $('#rechargeReport').addClass('reportsActive');
            }
            else if (/waterLevel/.test(location)) {
                $(".collapse-item").removeClass("reportsActive")
                $('#waterLevel').addClass('reportsActive');
            }
            else if (/salebyrfid/.test(location)) {
                $(".collapse-item").removeClass("reportsActive")
                $('#reportsc').addClass('reportsActive');
            } else {
                $(".collapse-item").removeClass("reportsActive")
            }

        });
    }
    render() {
        const { result } = this.props;
        const props = result ? JSON.parse(result.userProperties) : "";
        console.log("Props:", props);
        return (
            <ul className="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
                <Link className="sidebar-brand d-flex align-items-center justify-content-center" to='/dashboard'>
                    {/* <div className="sidebar-brand-icon rotate-n-15">
                <i className="fas fa-laugh-wink"></i>
              </div>
              <div className="sidebar-brand-text mx-3">SB Admin <sup>2</sup></div> */}
                    <div className="sidebar-brand-icon">
                        {/* <img className="logoi" src="../public/img/logo.svg" width="210px" height="60px" /> */}
                    </div>
                </Link>
                <hr className="sidebar-divider my-0" />
                <li className="nav-item">
                    <Link className="nav-link" to='/dashboard' onClick={() => this.linkClicked()}>
                        <i className="fas fa-fw fa-tachometer-alt"></i>
                        <span>Dashboard</span></Link>
                </li>
                <hr className="sidebar-divider" />
                <li className="nav-item">
                    <Link className="nav-link" to="/upload" onClick={() => this.linkClicked()}>
                        <i className="fas fa-cog"></i>
                        <span>Upload File</span></Link>
                </li>
                <hr className="sidebar-divider" />
                <li className="nav-item">
                    <Link className="nav-link" to="/data" onClick={() => this.linkClicked()}>
                        <i className="fas fa-cog"></i>
                        <span>Uploaded Data</span></Link>
                </li>
            </ul >
        );
    }
}

const mapStateToProp = (state) => {
    return {
        result: state.ntrs.auth.user.result,
    }
};

export default connect(mapStateToProp)(LeftMenu);
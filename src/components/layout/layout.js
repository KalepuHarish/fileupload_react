import React, { Component } from 'react'
import { connect } from 'react-redux';
import PureRenderMixin from 'react-addons-pure-render-mixin'
import * as actionCreators from '../../actions/ntrs'
import Header from './header'
import Footer from './footer'
import LeftMenu from './leftmenu'
import Dashboard from '../dashboard/dashboard'
import Upload from '../upload/Upload'
import UploadData from '../upload/UploadData'

import { Switch, Route, } from 'react-router-dom'

class Layout extends Component {
    constructor(props) {
        super(props);
        this.logout = this.logout.bind(this);
        this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
    }

    logout() {
        this.props.logout()
    }

    render() {
        return (
            <div>
                <div id="wrapper">
                    <LeftMenu />
                    <div id="content-wrapper" className="d-flex flex-column">
                        <div id="content">
                            <Header />
                            <Switch>
                                <Route exact path='/dashboard' component={Dashboard} />
                                <Route exact path='/upload' component={Upload} />
                                <Route exact path='/data' component={UploadData} />
                            </Switch>
                        </div>
                        <Footer />
                    </div>
                </div>
                {/* <a className="scroll-to-top rounded" href="#page-top">
                    <i className="fas fa-angle-up"></i>
                </a> */}
                <div className="modal fade" id="logoutModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                                <button className="close" type="button" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div className="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                            <div className="modal-footer">
                                <button className="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                                <button className="btn btn-primary" onClick={this.logout} type="button" data-dismiss="modal">Logout</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProp = (state) => {
    return {
        result: state.ntrs.auth.user.result,
        loggedIn: state.ntrs.auth.loggedIn,
        message: state.ntrs.auth.message,
        persist: state
    }
};

export default connect(mapStateToProp, actionCreators)(Layout);
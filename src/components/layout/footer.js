import React, { Component } from 'react'
import { connect } from 'react-redux';

class Footer extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <footer className="sticky-footer bg-white" >
                <div className="container my-auto">
                    <div className="copyright text-center my-auto">
                        <span> Copyright &copy; 2018-2026 File Upload.. All rights reserved.</span>
                    </div>
                </div>
            </footer>
        );
    }
}

const mapStateToProp = (state) => {
    return {
        news: state.news
    }
};

export default connect(mapStateToProp)(Footer);
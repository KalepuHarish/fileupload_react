import React, { Component } from 'react'
import { connect } from 'react-redux';
import * as actionCreators from '../../actions/ntrs'

class Contact extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: null
        }
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="card" id="card">
                    <div className="card-body">
                        <h4><b>For Complaints / Suggestions :</b><hr/></h4>
                        <ul>
                            <li>+91-9100466466</li>
                            <li>+91-9100477477</li>
                            <li>Email-ID: pelmireengineering@gmail.com</li>
                        </ul>
                        <br/>
                        <h4><b>For New Cards / Recharges :</b><hr/></h4>
                        <ul>
                            <li>+91-9390199932</li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProp = (state) => {
    return {
        userDetails: state.ntrs.auth.user.result
    }
};

export default connect(mapStateToProp, actionCreators)(Contact);
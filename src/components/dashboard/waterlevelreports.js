import React, { Component } from 'react'
import { connect } from 'react-redux';
//import ReactApexChart from "react-apexcharts"
//import DonutChart from 'react-donut-chart';
import { Doughnut } from 'react-chartjs-2';
import { Link } from 'react-router-dom'

class WaterLevelreports extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: {
               
                datasets: [{
                    data: [200, 200],
                    backgroundColor: [
                        '#D8E9FB',
                        '#50C9F2'
                    ],
                    hoverBackgroundColor: [
                        '#D8E9FB',
                        '#50C9F2'
                    ]
                }],
                labels: [
                    'Water Produced',
                    'Water Dispenced'
                ]
            }
        };
    }
    componentDidMount() {
        //  console.log(this.chartReference); // returns a Chart.js instance reference
    }

    render() {
        return (
            <div className="water_levelcont container-fluid">
                <div className="row">
                    <div className="col-md-9">
                        <Doughnut ref={this.chartReference} data={this.state.data} />
                        <div id="chart">

                            {/* <DonutChart
                        data={[{
                            label: 'Give you up',
                            value: 50
                            
                        },
                        {
                            label: 'Harish Kalepu',
                            value: 50,
                        },
                        ]} colors={['#D8E9FB','#50C9F2']} height={400} width={400}/> */}

                        </div>
                    </div>
                    <div className="col-md-3">
                        <div className="row no-gutters align-items-center">
                            <ul className="revenue-ulfirst  chart_revenue-ul">
                                <li>
                                    <p className="revenue-firstp">Average TDS</p>
                                    <div className="revenue-secondp h5 mb-0 font-weight-bold text-gray-800">{this.state.data ? this.state.data.cluster ? this.state.data.cluster.downClusters : '0' : "0"}</div>
                                </li>
                                </ul>

                                <ul className="revenue-ulfirst  chart_revenue-ul">
                                <li>
                                    <p className="revenue-firstp">Average pH</p>
                                    <div className="revenue-secondp h5 mb-0 font-weight-bold text-gray-800">{this.state.data ? this.state.data.cluster ? this.state.data.cluster.downClusters : '0' : "0"}</div>
                                </li></ul>
                        </div>
                    </div>
                </div>

                {/* <hr />
                <div className="row no-gutters align-items-center">
                    <div className="col">
                        <ul className="list-inline revenue-ul">
                           
                            <li>
                               
                            </li>
                        </ul>
                    </div>
                </div> */}
                {/* <hr />
                <div className="row no-gutters align-items-center">
                    <div className="col viewdet-waterlevl">
                        <Link to="/dashboard"><h5>Veiw Details</h5></Link>
                    </div>
                </div> */}
            </div>
        );
    }
}

const mapStateToProp = (state) => {
    return {
        userDetails: state.ntrs.auth.user.result
    }
};

export default connect(mapStateToProp)(WaterLevelreports);
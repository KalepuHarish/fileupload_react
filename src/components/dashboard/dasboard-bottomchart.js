import React, { Component } from 'react'
import { connect } from 'react-redux';
import Chart from "react-apexcharts"
// import { Doughnut } from 'react-chartjs-2';

class Dasboardbottomchart extends Component {
    constructor(props) {
        super(props)
        this.state = {
            options: {
                chart: {
                    id: "basic-bar"
                },
                xaxis: {
                    categories: [1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998]
                }
            },
            series: [
                {
                    name: ["series-1", "series-2"],
                    data: [30, 40, 45, 50, 49, 60, 70, 91]
                }
            ]
        };
    }
    componentDidMount() {
        //  console.log(this.chartReference); // returns a Chart.js instance reference
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row dsh-main-swip12">
                    <div className="col-md-6">
                        <h4> <span>OVER ALL SALES & WATER QUALITY REPORTS</span></h4>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6">
                        <div id="chart">
                            <Chart
                                options={this.state.options}
                                series={this.state.series}
                                type="bar"
                                width="100%"
                            />
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div id="chart">
                            <Chart
                                options={this.state.options}
                                series={this.state.series}
                                type="bar"
                                width="100%"
                            />
                        </div>
                    </div>
                </div>
                {/* <Doughnut ref={this.chartReference} data={data} /> */}

            </div>
        );
    }
}

const mapStateToProp = (state) => {
    return {
        userDetails: state.ntrs.auth.user.result
    }
};

export default connect(mapStateToProp)(Dasboardbottomchart);
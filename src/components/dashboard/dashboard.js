import React, { Component } from 'react'
import { connect } from 'react-redux';
import * as actionCreators from '../../actions/ntrs';
import WaterLevelreports from './waterlevelreports';
import Dasboardbottomchart from './dasboard-bottomchart';
const Anusha_Kb_mugitu = 'https://doc-08-ao-docs.googleusercontent.com/docs/securesc/1prdmmtueeni1v953hkk0s0s33ru09ht/fuiosp5phncmd06occ6qahrve256rmfr/1588419450000/18291397945631664867/18291397945631664867/15IBJ-5phdv0fzi9v530F9pDECwKk_-J6?e=download&authuser=0&nonce=t7fjrg0bld2hk&user=18291397945631664867&hash=57m8b6i2nnf11q64dfjh1mu1kqqc51ta';

class Dashboard extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
        this.search = this.search.bind(this);
        this.back = this.back.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.resetData = this.resetData.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    back() {

    }

    search() {
    }

    onSubmit() {

    }

    resetData() {

    }

    componentWillMount() {

    }

    handleChange(e) {

    }

    render() {
        const { userDetails } = this.props;
        return (
            <div className="container-fluid">
                <div className="row main_body_pad">
                    <div className="col-md-7 dash_board_top_sec">
                        <h1>Welcome, <span><b>{userDetails.name}</b></span></h1>
                        <div>
                            {/*<hr className="user-status_dash" />*/}

                        </div>
                        {/*<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the Monday, 5.30 Am</p> */}
                        <h1>Rules to upload file:</h1>
                        <ul>
                            <li>Please download the template below. dont change the headers in template.</li>
                            <li>File naming convention should be same as for ex: name1.name2.name3.xlsx</li>
                        </ul>
                        <button><a href={Anusha_Kb_mugitu} download="Anusha_Kb_mugitu">Download</a></button>
                    </div>


                </div>
            </div>
        );
    }
}

const mapStateToProp = (state) => {
    return {
        userDetails: state.ntrs.auth.user.data
    }
};

export default connect(mapStateToProp, actionCreators)(Dashboard);
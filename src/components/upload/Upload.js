import React from 'react';
import moment from 'moment'
import PureRenderMixin from 'react-addons-pure-render-mixin'
import { connect } from 'react-redux'
import * as actionCreators from '../../actions/ntrs'
import Dialog from 'react-bootstrap-dialog'

class Upload extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedFile: null
        };
        this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
         this.uploadfile = this.uploadfile.bind(this);

    }

    onSubmit(e) {
        e.preventDefault();
        // this.props.getRdu(object).then(result => {
        //     const rdu = result;
        //     if (rdu.result) {
        //         this.setState({ rduData: rdu.result });
        //     }
        // });
    }

    handleChange(e) {
        e.preventDefault();
        this.setState({
            selectedFile: e.target.files[0]
        });
        /* const formData = new FormData();
          var name = e.target.files[0].name;
          var array = name.split(".");
          var length = array.length;
          if (length === 4) {
              formData.append('file', e.target.files[0]);
              this.props.upload(formData).then(response => {
                  if(response.status === 400) {
                      alert(response.message);
                  } else {
                      alert("File uploaded successfully.")
                  }
                  this.myFormRef.reset();
              });
          }
          else {
              alert("Please upload file with valid name")
          }*/

        // fetch('http://localhost:8080/api/upload', {
        //     method: 'post',
        //     body: formData
        // }).then(res => {
        //     console.log(res);
        //     if(res.ok) {
        //         console.log(res.data);
        //         alert("File uploaded successfully.")
        //     }
        // });
    }

    uploadfile() {
       
        const formData = new FormData();
        var name = this.state.selectedFile.name;
        var array = name.split(".");
        var length = array.length;
        if (length === 4) {
            formData.append('file', this.state.selectedFile);
            this.props.upload(formData).then(response => {
                if (response.status === 400) {
                    alert(response.message);
                } else {
                    alert("File uploaded successfully.")
                }
                this.myFormRef.reset();
            });
        }
        else {
            alert("Please upload file with valid name")
        }
    }

    render() {

        return (
            <div className="container-fluid">
                <section className="box-header">
                </section>
                <section className="content">
                    <div className="row">
                        <div className="col-lg-12 col-xs-24">
                            <div>
                                <div className="box card">
                                    <form role="form"  ref={(el) => this.myFormRef = el} onSubmit={this.onSubmit.bind(this)}>
                                        <div className="box-body card-body top-navs">
                                            <div className="row filter-top-bar">
                                                <div className="col-md-12">
                                                    <h4>Upload File Here</h4>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col files color">
                                                    <input type="file" style={{width:"90%"}}  name="file" onChange={this.handleChange}></input>
                                                    <button onClick={this.uploadfile}>Upload</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        list: state.ntrs.auth.user.data
    }
}

export default connect(mapStateToProps, actionCreators)(Upload);
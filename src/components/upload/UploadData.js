import React, { Component } from 'react'
import { connect } from 'react-redux';
import * as actionCreators from '../../actions/ntrs'
import { MDBDataTable } from 'mdbreact';
import PureRenderMixin from 'react-addons-pure-render-mixin'
import DatePicker from "react-datepicker";
import moment from 'moment'
import "react-datepicker/dist/react-datepicker.css";

class UploadData extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: []
        };
        this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        
    }

    handleChange(e)
    {
        console.log("targetvalue", e.target.value)
		if(e.target.value != ""){
			const promise = this.props.getData(e.target.value);
			promise.then(response => {
				console.log(response)
				this.setState({ list: response.data });
			});
		} else {
			this.setState({ list: [] });
		}

    }

    render() {
        const { list } = this.state;
        const data = {
            columns: [
                { label: 'ID', field: 'id', sort: 'asc', width: 150 },
                { label: 'NAME', field: 'name', sort: 'asc', width: 270 },
                { label: 'ADDRESS', field: 'address', sort: 'asc', width: 270 }
            ],
            rows: []
        }
        for (var i = 0; i < list.length; i++) {
            data.rows.push(list[i]);
        }
        return (
            <div className="container-fluid">
                <div style={{paddingBottom: "50px"}}>
                 <input type="text" className="form-control" name="search" placeholder="Search...."onChange={this.handleChange} />
                 </div>
                <MDBDataTable
                    striped
                    bordered
                    hover
                    data={data}
                    entriesOptions={[5, 10]}
                    entries={10}
                    pagesAmount={5}
                    sortable={true}
                    small={true}
                />
            </div>
        );
    }
}

const mapStateToProp = (state) => {
    return {
        list: state.ntrs.auth.user.data
    }
};

export default connect(mapStateToProp, actionCreators)(UploadData);